��    J      l  e   �      P     Q     Z  	   l     v     ~  	   �     �     �     �     �     �     �          %     A     M     Y     a     i     u     �     �     �     �     �     �     �     �  S   �  T   H  T   �  T   �  H   G	  I   �	     �	     �	     �	     
  
   
     
     "
     ;
     H
     U
     b
  	   n
     x
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
               '     5     N  	   a     k     {     �     �     �     �     �     �     �     �  u  �  
   n     y     �     �     �     �  	   �     �     �               ,     F     a     {     �     �  	   �     �     �     �     �     �     �          $     B     N  W   g  W   �  X     W   p  W   �  X         y     ~     �     �  
   �     �     �     �     �     �               *     E     M     Z     h     q          �     �     �     �     �  	   �     �  !        1  	   L     V  
   d     o     t     |     �     �     �     �     �                      %      $   -                  4       !              9              D   F      3             /   ,   	             (            #                             @   7   &   2   1      *   :   J   +       
   H         C          <   ?   5       6           .       '   0   I                      E   B   >   8   G               )          A   "      =   ;    About Us Add Another Slide Add Image Add New Add New Service Add Video Address All Services Background Color Background Image Background Image (Landscape) Background Image (Portrait) Background Video (Landscape) Background Video (Portrait) Button Text Button call Contact Content Description Edit Service Email Facebook (URL) Featured Image Filter services list Footer Address Footer Map Link (URL) Form Form Shortcode Format: JPG (recommended), PNG | Recommended size: 1080x1920px | Aspect ratio: 9:16 Format: JPG (recommended), PNG | Recommended size: 1440x1392px | Aspect ratio: 30:29 Format: JPG (recommended), PNG | Recommended size: 2560x1080px | Aspect ratio: 64:27 Format: JPG (recommended), PNG | Recommended size: 2880x1000px | Aspect ratio: 72:25 Format: MP4 (muted) | Recommended size: 1080x1920px | Aspect ratio: 9:16 Format: MP4 (muted) | Recommended size: 2560x1080px | Aspect ratio: 64:27 Hero Insert into service Instagram (URL) Link Link (URL) LinkedIn (URL) Looping Background Video MM2 Services Map (iframe) Mobile Phone New Service Not found Not found in Trash Page Page Target Parent Service: Phone Remove Slide Remove featured image Search Service Select target Service Service Archives Service Attributes Services Services list Services list navigation Set featured image Slide {#} Social Networks Subtitle Target Title Update Service Uploaded to this service Use as featured image View Service View Services Youtube (URL) Project-Id-Version: 
PO-Revision-Date: 2021-12-24 18:20-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: mm2assessoriaeconsultoriacombr.php
 Sobre Nós Adicionar Novo Slide Adicionar imagem Adicionar Novo Adicionar Novo Serviço Adicionar Vídeo Endereço Todos os Serviços Cor de fundo Imagem de fundo Imagem de fundo (Paisagem) Imagem de fundo (Retrato) Vídeo de fundo (Paisagem) Vídeo de fundo (Retrato) Texto do Botão Chamada do Botão Contato Conteúdo Descrição Editar Serviço E-mail Facebook (URL) Imagem em destaque Filtrar lista de serviços Endereço no Rodapé Link do Mapa no Rodapé (URL) Formulário Shortcode do Formulário Formato: JPG (recomendado), PNG | Tamanho recomendado: 1080x1920px | Aspect ratio: 9:16 Formato: JPG (recomendado), PNG | Tamanho recomendado: 1440x1392px | Proporção: 30:29 Formato: JPG (recomendado), PNG | Tamanho recomendado: 2560x1080px | Aspect ratio: 64:27 Formato: JPG (recomendado), PNG | Tamanho recomendado: 2880x1000px | Proporção: 72:25 Formato: JPG (recomendado), PNG | Tamanho recomendado: 1080x1920px | Aspect ratio: 9:16 Formato: JPG (recomendado), PNG | Tamanho recomendado: 2560x1080px | Aspect ratio: 64:27 Hero Inserir no serviço Instagram (URL) Link Link (URL) LinkedIn (URL) Vídeo de fundo em looping Serviços da MM2 Mapa (iframe) Telefone Celular Novo Serviço Não encontrado Não encontrado na lixeira Página Página Alvo Serviço Pai: Telefone Remover Slide Remover imagem em destaque Pesquisar Serviço Selecione o alvo Serviço Arquivo de Serviços Atributos do serviço Serviços Lista de Serviços Navegação na lista de serviços Definir imagem em destaque Slide {#} Redes Sociais Subtítulo Alvo Título Atualizar Serviço Enviado para este serviço Usar como imagem destacada Ver Serviço Ver os Serviços Youtube (URL) 