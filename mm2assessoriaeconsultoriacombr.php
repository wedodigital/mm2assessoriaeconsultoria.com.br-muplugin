<?php
/**
 * MM2 Assessoria e Consultoria - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   MM2 Assessoria e Consultoria
 * @author    WeDo Digital <contato@wedodigital.com.br>
 * @copyright 2021 WeDo Digital
 * @license   Proprietary https://wedodigital.com.br
 * @link      https://wedodigital.com.br
 *
 * @wordpress-plugin
 * Plugin Name: MM2 Assessoria e Consultoria - mu-plugin
 * Plugin URI:  https://wedodigital.com.br
 * Description: Customizations for mm2assessoriaeconsultoria.com.br site
 * Version:     1.0.0
 * Author:      WeDo Digital
 * Author URI:  https://wedodigital.com.br/
 * Text Domain: mm2assessoriaeconsultoriacombr
 * License:     Proprietary
 * License URI: https://wedodigital.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('mm2assessoriaeconsultoriacombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**************************************************************************************************************
 * Functions
 **************************************************************************************************************/


/**
 * Get a list of posts
 *
 * Generic function to return an array of posts formatted for CMB2. Simply pass
 * in your WP_Query arguments and get back a beautifully formatted CMB2 options
 * array.
 *
 * @param array $query_args WP_Query arguments
 * @return array CMB2 options array
 */
function Get_Post_array($query_args = array(), $exclude_current_post = false, $add_none = false) 
{
    $lang = function_exists('pll_current_language') && isset($_GET['post']) ? pll_get_post_language($_GET['post']) : '';
    $exclude_id = $exclude_current_post && isset($_GET['post']) ? array($_GET['post']) : '';

    $defaults = array(
        'posts_per_page' => -1,
        'orderby'          => 'title',
        'order'            => 'ASC',
        'post_status'      => 'publish',
        'post__not_in'     => $exclude_id,
        'lang'             => $lang
    );
    $query = new WP_Query(array_replace_recursive($defaults, $query_args));
    
    if ($add_none) {
        return array("-1" => '') + wp_list_pluck($query->get_posts(), 'post_title', 'ID');
    } else {
        return wp_list_pluck($query->get_posts(), 'post_title', 'ID');
    }
    
}

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

/**********
 * Show on Front Page
 *
 * @return bool $display
 */
function Show_On_Front_page($cmb)
{
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($front_page) : $front_page;

    // There is a front page set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Home / Blog
 *
 * @return bool $display
 */
function Show_On_Home($cmb)
{
    // Get ID of page set as home, 0 if there isn't one
    $home = get_option('page_for_posts');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($home) : $home;

    // There is a home set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Privacy Policy 
 *
 * @return bool $display
 */
function Show_On_Privacy($cmb)
{
    // Get ID of page set as privacy, 0 if there isn't one
    $privacy = get_option('wp_page_for_privacy_policy');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($privacy) : $privacy;

    // There is a privacy set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}


/**
 * Show on Plan
 *
 * @return bool $display
 *
 * @author Tom Morton
 * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
 */
function Cmb2_Show_On_plan()
{
    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return false;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    $slugs = array("combos", "internet", "tv"); 

    $parents = get_post_ancestors($post_id);

    return in_array($slug, $slugs) AND (!$parents);
}

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    $parent_slug = true;
    if (isset($meta_box['show_on']['parant_slug'])) {
        $parents = get_post_ancestors($post_id); 
        $id = ($parents) ? $parents[count($parents)-1]: false;
        /* Get the parent and set the $class with the page slug (post_name) */
        if ($id) {
            $parent = get_post($id);
            $parent_slug = $parent->post_name == $meta_box['show_on']['parent_slug'];
        }
    }

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']) AND $parent_slug;
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Removes metabox from appearing on post new screens before the post
 * ID has been set.
 *
 * @param bool  $display
 * @param array $meta_box The array of metabox options
 * 
 * @return bool $display True on success, false on failure
 * 
 * @author Thomas Griffin
 */
function Cmb2_Exclude_From_New($display, $meta_box)
{
    if (!isset($meta_box['show_on']['alt_key'], $meta_box['show_on']['alt_value'])) {
        return $display;
    }

    if ('exclude_new' !== $meta_box['show_on']['alt_key']) {
        return $display;
    }

    global $pagenow;

    // Force to be an array
    $to_exclude = !is_array($meta_box['show_on']['alt_value'])
        ? array($meta_box['show_on']['alt_value'])
        : $meta_box['show_on']['alt_value'];

    $is_new_post = 'post-new.php' == $pagenow && in_array('post-new.php', $to_exclude);

    return ! $is_new_post;
}
add_filter('cmb2_show_on', 'Cmb2_Exclude_From_New', 10, 2);

/**
 * Exclude metabox on non top level posts
 *
 * @param bool $display
 * @param array $meta_box
 * 
 * @return bool display metabox
 * 
 * @author Travis Northcutt
 * @link   https://gist.github.com/gists/2039760
 */
function Cmb2_Metabox_Add_For_Top_Level_Posts_only($display, $meta_box)
{
    if (!isset($meta_box['show_on']['top_key']) || 'parent-id' !== $meta_box['show_on']['top_key']) {
        return $display;
    }

    $post_id = 0;
    
    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }
    
    if (!$post_id) {
        return $display;
    }
    
    // If the post doesn't have ancestors, show the box
    if (!get_post_ancestors($post_id)) {
        return $display;
    }
    
    // Otherwise, it's not a top level post, so don't show it
    return false;
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Add_For_Top_Level_Posts_only', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Register taxonomies
 * ********************************************************************************/

/***********************************************************************************
 * Register post types
 * ********************************************************************************/
/*****
 * Services
 */
function cpt_mm2_service() 
{
    $labels = array(
        'name'                  => __('Services', 'mm2assessoriaeconsultoriacombr'),
        'singular_name'         => __('Service', 'mm2assessoriaeconsultoriacombr'),
        'menu_name'             => __('Services', 'mm2assessoriaeconsultoriacombr'),
        'name_admin_bar'        => __('Service', 'mm2assessoriaeconsultoriacombr'),
        'archives'              => __('Service Archives', 'mm2assessoriaeconsultoriacombr'),
        'attributes'            => __('Service Attributes', 'mm2assessoriaeconsultoriacombr'),
        'parent_item_colon'     => __('Parent Service:', 'mm2assessoriaeconsultoriacombr'),
        'all_items'             => __('All Services', 'mm2assessoriaeconsultoriacombr'),
        'add_new_item'          => __('Add New Service', 'mm2assessoriaeconsultoriacombr'),
        'add_new'               => __('Add New', 'mm2assessoriaeconsultoriacombr'),
        'new_item'              => __('New Service', 'mm2assessoriaeconsultoriacombr'),
        'edit_item'             => __('Edit Service', 'mm2assessoriaeconsultoriacombr'),
        'update_item'           => __('Update Service', 'mm2assessoriaeconsultoriacombr'),
        'view_item'             => __('View Service', 'mm2assessoriaeconsultoriacombr'),
        'view_items'            => __('View Services', 'mm2assessoriaeconsultoriacombr'),
        'search_items'          => __('Search Service', 'mm2assessoriaeconsultoriacombr'),
        'not_found'             => __('Not found', 'mm2assessoriaeconsultoriacombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'mm2assessoriaeconsultoriacombr'),
        'featured_image'        => __('Featured Image', 'mm2assessoriaeconsultoriacombr'),
        'set_featured_image'    => __('Set featured image', 'mm2assessoriaeconsultoriacombr'),
        'remove_featured_image' => __('Remove featured image', 'mm2assessoriaeconsultoriacombr'),
        'use_featured_image'    => __('Use as featured image', 'mm2assessoriaeconsultoriacombr'),
        'insert_into_item'      => __('Insert into service', 'mm2assessoriaeconsultoriacombr'),
        'uploaded_to_this_item' => __('Uploaded to this service', 'mm2assessoriaeconsultoriacombr'),
        'items_list'            => __('Services list', 'mm2assessoriaeconsultoriacombr'),
        'items_list_navigation' => __('Services list navigation', 'mm2assessoriaeconsultoriacombr'),
        'filter_items_list'     => __('Filter services list', 'mm2assessoriaeconsultoriacombr'),
    );
    $rewrite = array(
        'slug'                  => 'servicos',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Service', 'mm2assessoriaeconsultoriacombr'),
        'description'           => __('MM2 Services', 'mm2assessoriaeconsultoriacombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-text-page',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'post',
    );
    register_post_type('mm2_service', $args);
}
add_action('init', 'cpt_mm2_service', 0);


/***********************************************************************************
 * CPT Fields
 * ********************************************************************************/
/**
 * Service
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_mm2_service_';
  
        /**
         * Hero
        */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => '_mm2_service_hero_id',
                'title'         => __('Hero', 'mm2assessoriaeconsultoriacombr'),
                'object_types'  => array('mm2_service'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
            
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'mm2assessoriaeconsultoriacombr'),
                    'add_button'   =>__('Add Another Slide', 'mm2assessoriaeconsultoriacombr'),
                    'remove_button' =>__('Remove Slide', 'mm2assessoriaeconsultoriacombr'),
                    'sortable'      => true, // beta
                ),
            )
        );
    
        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'mm2assessoriaeconsultoriacombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'mm2assessoriaeconsultoriacombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'mm2assessoriaeconsultoriacombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    => __('Background Image (Portrait)', 'mm2assessoriaeconsultoriacombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'mm2assessoriaeconsultoriacombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'mm2assessoriaeconsultoriacombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 210)
            )
        );

        //Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );
    
        /******
         * Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => '_mm2_service_content_id',
                'title'         => __('Content', 'mm2assessoriaeconsultoriacombr'),
                'object_types'  => array('mm2_service'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

          //Content
        $cmb_content->add_field(
            array(
                'name'       => __('Content', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => true,
                    'textarea_rows' => get_option('default_post_edit_rows', 25),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );
    }
);
  
/***********************************************************************************
 * Page Fields
 * ********************************************************************************/

 /**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_mm2_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => '_mm2_frontpage_hero_id',
                'title'         => __('Hero', 'mm2assessoriaeconsultoriacombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'mm2assessoriaeconsultoriacombr'),
                    'add_button'   =>__('Add Another Slide', 'mm2assessoriaeconsultoriacombr'),
                    'remove_button' =>__('Remove Slide', 'mm2assessoriaeconsultoriacombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#fbfbfb',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#fbfbfb', '#ffffff'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'mm2assessoriaeconsultoriacombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'mm2assessoriaeconsultoriacombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'mm2assessoriaeconsultoriacombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'mm2assessoriaeconsultoriacombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'mm2assessoriaeconsultoriacombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'mm2assessoriaeconsultoriacombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'mm2assessoriaeconsultoriacombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'mm2assessoriaeconsultoriacombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'mm2assessoriaeconsultoriacombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 210)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'mm2assessoriaeconsultoriacombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'mm2assessoriaeconsultoriacombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'mm2assessoriaeconsultoriacombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text',
            )
        );

        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => __('Button call', 'mm2assessoriaeconsultoriacombr'),
                'id'         => 'btn_text',
                'type'       => 'text',
            )
        );

        //Hero Target
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Target', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => __('Select target', 'mm2assessoriaeconsultoriacombr'),
                'id'         => 'btn_target',
                'type'       => 'radio',
                'show_option_none' => false,
                'options'          => array(
                    'page'   => __('Page', 'mm2assessoriaeconsultoriacombr'),
                    'link'  => __('Link (URL)', 'mm2assessoriaeconsultoriacombr'),
                ),
                'default' => 'page',
            )
        );

        //Hero Target Page
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Page Target', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => 'target_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        //Hero Link
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Link', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => 'target_link_url',
                'type'       => 'text',
            )
        );

        /**
        * About Us
        */
        $cmb_aboutus = new_cmb2_box(
            array(
                'id'            => '_mm2_frontpage_aboutus_id',
                'title'         => __('About Us', 'mm2assessoriaeconsultoriacombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Title
        $cmb_aboutus->add_field(
            array(
                'name'       => __('Title', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'aboutus_title',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Description
        $cmb_aboutus->add_field(
            array(
                'name'       => __('Description', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'aboutus_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_aboutus->add_field(
            array(
                'name'       => __('Button Text', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => __('Button call', 'mm2assessoriaeconsultoriacombr'),
                'id'         => $prefix . 'aboutus_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_aboutus->add_field(
            array(
                'name'       => __('Page Target', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'aboutus_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        /**
        * Services
        */
        $cmb_services = new_cmb2_box(
            array(
                'id'            => '_mm2_frontpage_services_id',
                'title'         => __('Services', 'mm2assessoriaeconsultoriacombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Background Image
        $cmb_services->add_field(
            array(
                'name'    => __('Background Image', 'mm2assessoriaeconsultoriacombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2880x1000px | Aspect ratio: 72:25', 'mm2assessoriaeconsultoriacombr'),
                'id'      => $prefix .'services_bkg_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'mm2assessoriaeconsultoriacombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(288, 100) 
            ) 
        );

        //Title
        $cmb_services->add_field(
            array(
                'name'       => __('Title', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_services->add_field(
            array(
                'name'       => __('Description', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_services->add_field(
            array(
                'name'       => __('Button Text', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => __('Button call', 'mm2assessoriaeconsultoriacombr'),
                'id'         => $prefix . 'services_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_services->add_field(
            array(
                'name'       => __('Page Target', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        /**
        * Contact
        */
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => '_mm2_frontpage_contact_id',
                'title'         => __('Contact', 'mm2assessoriaeconsultoriacombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Featured Image
        $cmb_contact->add_field(
            array(
                'name'    => __('Featured Image', 'mm2assessoriaeconsultoriacombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1440x1392px | Aspect ratio: 30:29', 'mm2assessoriaeconsultoriacombr'),
                'id'      => $prefix .'contact_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'mm2assessoriaeconsultoriacombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(144, 139) 
            )
        ); 

        //Title
        $cmb_contact->add_field(
            array(
                'name'       => __('Title', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_title',
                'type'       => 'text',
            )
        );

        //Description
        $cmb_contact->add_field(
            array(
                'name'       => __('Description', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_desc',
                'type'       => 'wysiwyg',
                'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 5),
                    'tabindex' => '',
                    'editor_css' => '',
                    'editor_class' => '',
                    'teeny' => true,
                    'tinymce' => true,
                    'quicktags' => true
                ),
            )
        );

        //Button Text
        $cmb_contact->add_field(
            array(
                'name'       => __('Button Text', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => __('Button call', 'mm2assessoriaeconsultoriacombr'),
                'id'         => $prefix . 'contact_btn_text',
                'type'       => 'text',
            )
        );

        //Page Target
        $cmb_contact->add_field(
            array(
                'name'       => __('Page Target', 'mm2assessoriaeconsultoriacombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_btn_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );
    }
);
